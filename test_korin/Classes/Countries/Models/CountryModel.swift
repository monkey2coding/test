//
//  Country.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation

struct Country: Decodable {
	let name: String
	let capital: String
	let currencies: [Currency]
	let languages: [Language]
	let flag: String
}

extension Country {
	var flagUrl: URL? {
		return URL(string: self.flag)
	}
}
