//
//  CurrencyModel.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation

struct Currency: Decodable {
	let code: String?
	let name: String?
	let symbol: String?
}
