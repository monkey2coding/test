//
//  Language.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation

struct Language: Decodable {
	let name: String
	let nativeName: String
}
