//
//  CountryDetailViewController.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import UIKit

class CountryDetailViewController: UIViewController {

	// OUTLETS
	@IBOutlet private var flagImageView: UIImageView!
	@IBOutlet private var countryNameLabel: UILabel!
	@IBOutlet private var currensiesListLabel: UILabel!
	@IBOutlet private var languagesListLabel: UILabel!
	
	var country: Country? = nil
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.country.flatMap(self.setup)
	}
	
	func setup(with country: Country) {
		self.title = self.country?.name
		self.countryNameLabel.text = country.capital
		self.currensiesListLabel.text = country.currencies.map { ($0.name ?? "") + " (" + ($0.symbol ?? "") + ")"  }.joined(separator: " ,")
		self.languagesListLabel.text = country.languages.map { $0.name }.joined(separator: " ,")
		country.flagUrl.flatMap(self.setupFlag)
	}

	private func setupFlag(with url: URL) {
		self.flagImageView.loadSVG(with: url)
	}
}
