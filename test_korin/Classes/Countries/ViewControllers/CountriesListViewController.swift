//
//  CountriesListViewController.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

class CountriesListViewController: UIViewController {
	var dispose = DisposeBag()
	
	private var countriesService = MoyaProvider<CountriesTarget>()

	// OUTLETS
	@IBOutlet private weak var countriesListTableView: CountriesListTableView!

	// PROPERTIES
	var showDetailCountry: ReplaySubject<Country?> =  ReplaySubject.create(bufferSize: 1)

	override func viewDidLoad() {
			super.viewDidLoad()
			self.prepare()
	}

	private func prepare() {
		self.bind()
		self.prepareUI() 
	}

	private func prepareUI() {
		self.title = "Countries"
	}

	private func bind() {
		self.countriesService
			   .getCountries()
			   .bind(to: self.countriesListTableView.countries)
		       .disposed(by: self.dispose)
		
		self.countriesListTableView
			   .didSelect
		       .bind(to: self.showDetailCountry)
		       .disposed(by: self.dispose)
	}
}

