//
//  CountriesListTableView.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CountriesListTableView: UITableView {
	var dispose = DisposeBag()

	// Properties
	var countries: BehaviorRelay<[Country]> = BehaviorRelay(value: [])
	var didSelect: BehaviorSubject<Country?> =  BehaviorSubject(value: nil)

	override func awakeFromNib() {
		super.awakeFromNib()
		self.prepare()
	}

	private func prepare() {
		self.prepareTableView()
		self.bind()
	}

	private func bind() {
		self.countries
			.asObservable()
			.map { _ in () }
			.subscribe(onNext: self.reloadData)
			.disposed(by: self.dispose)
	}

	private func prepareTableView() {
		self.delegate = self
		self.dataSource = self
		self.register(nibWithCellClass: CountryTableViewCell.self)
	}

}

extension CountriesListTableView: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.countries.value.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		return tableView.dequeueReusableCell(for: indexPath) { (cell: CountryTableViewCell) in
			let countries =  self.countries.value
			cell.setup(with: countries[indexPath.row])
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 130
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		self.didSelect.onNext(self.countries.value[indexPath.row])
	}
}
