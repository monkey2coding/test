//
//  CountryTableViewCell.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import UIKit
import WebKit

class CountryTableViewCell: UITableViewCell {

	// OUTLETS
	@IBOutlet private var flagImageView: UIImageView!
	@IBOutlet private var currencyLabel: UILabel!
	@IBOutlet private var countryLabel: UILabel!
	@IBOutlet private var capitalLabel: UILabel!

	func setup(with country: Country) {
		self.countryLabel.text = country.name
		self.currencyLabel.text = country.currencies.compactMap { $0.symbol }.joined(separator: ", ")
		self.capitalLabel.text = country.capital
		country.flagUrl.flatMap(self.setupFlag)
	}

	private func setupFlag(with url: URL) {
		self.flagImageView.loadSVG(with: url)
	}

}
