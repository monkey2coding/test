//
//  CountriesService.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation
import Moya
import RxSwift

extension MoyaProvider where MoyaProvider .Target == CountriesTarget {

	func getCountries() -> Observable<[Country]> {
		return self.rx
							.request(.list)
							.map([Country].self)
							.asObservable()
	}
}
