//
//  CountriesCoordinator.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation
import UIKit.UIViewController
import RxSwift
import RxCocoa

class CountriesCoordinator: CoordinatorProtocol {

	var navigationController: UINavigationController?
	var rootViewController = StoryboardScene.Countries.countriesListViewController.instantiate()

	required init() {
		self.prepare()
	}

	private func prepare() {
		self.bind()
	}

	private func bind() {
		self.rootViewController
		       .showDetailCountry
		       .asObservable()
			   .ignoreNil()
			   .subscribe(onNext: self.openCountryDetail)
		 	   .disposed(by: self.rootViewController.dispose)
	}

	func openCountryDetail(with country: Country) {
		let countryDetailController = StoryboardScene.Countries.countryDetailViewController.instantiate()
		countryDetailController.country = country
		self.navigationController?.pushViewController(countryDetailController,
													  animated: true)
	}
}
