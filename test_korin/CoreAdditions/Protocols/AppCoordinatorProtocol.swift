//
//  AppCoordinatorProtocol.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation
import UIKit.UINavigationController

protocol CoordinatorProtocol {
	associatedtype Controller where Controller: UIViewController
	var navigationController: UINavigationController? { get set }
	var rootViewController: Controller { get set }
	func start()
	init()
	init(navigation: UINavigationController)
}

extension CoordinatorProtocol {
	func start() {
		self.navigationController?.pushViewController(self.rootViewController, animated: true)
	}

	init(navigation: UINavigationController) {
		self.init()
		self.navigationController = navigation
	}
}
