//
//  Constants.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation

enum StringConstants: String {
	case apiHost = "https://restcountries.eu"
}
