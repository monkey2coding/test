//
//  UIImageView+SVG.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 04.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import UIKit.UIImageView
import WebKit

extension UIImageView {
	 func loadSVG(with url: URL) {
		self.subviews.filter { $0 is WKWebView  }.forEach { $0.removeFromSuperview() }
		DispatchQueue.global(qos: .background).async {
			if let svgString = try? String(contentsOf: url) {
				DispatchQueue.main.async {
					let webKit = WKWebView(frame: self.bounds)
					webKit.loadHTMLString(svgString, baseURL: nil)
					self.addSubview(webKit)
				}
			}
		}
	}
}

