//
//  Countries+TargetType.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation
import Moya

enum CountriesTarget {
	case list
	case country(name: String)
}

extension CountriesTarget: TargetType {
	var path: String {
		switch self {
		case .list: return "/rest/v2/all"
		case .country(let name): return "rest/v2/name/{\(name)}"
		}
	}

	var method: Moya.Method {
		return .get
	}

	var sampleData: Data {
		return Data()
	}
	
	var task: Task {
		switch self {
		case .list:
			return .requestPlain
		case .country:
			return .requestPlain
		}
	}
	
	var headers: [String : String]? {
		return nil
	}
	
	var baseURL: URL {
		guard let url = URL(string: StringConstants.apiHost.rawValue) else { fatalError() }
		return url
	}
}
