//
//  AppCoordinator.swift
//  test_korin
//
//  Created by Yaroslav Khodorovskyi on 03.08.2018.
//  Copyright © 2018 Yaroslav Khodorovskyi. All rights reserved.
//

import Foundation
import UIKit.UINavigationController

class AppCoordinator {
	lazy var navigationController = UINavigationController()

	init<T>(initialCoordinatorType: T.Type , window: UIWindow?) where T: CoordinatorProtocol {
		window?.rootViewController = self.navigationController
		let initialCoordinator = initialCoordinatorType.init(navigation: self.navigationController)
		initialCoordinator.start()
		self.prepare()
	}
	
	private func prepare() {

	}
}
